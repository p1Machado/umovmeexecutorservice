const _ = require("lodash");
const axios = require("axios").default;

axios.interceptors.request.use(config => {
  config.headers.authorization = "Basic cDFNYWNoYWRvOlNoaW4xMjA5QA==";
  return config;
}, Promise.reject);

let maxTasks = 5;

let executionPool = []; // max
let executionQueue = []; // ilimited

let acceptTasks = _.throttle(function() {
  while (executionPool.length < maxTasks && executionQueue.length > 0) {
    executionPool.push(executionQueue.shift());
  }
}, 1000);

function tickRate() {
  if (executionPool.length < maxTasks && executionQueue.length > 0) {
    acceptTasks();
  }
  executionPool.forEach(task => {
    if (!task.running) {
      task.running = true;
      Promise.resolve(task.execute()).then(() => {
        console.log(new Date());
        executionPool = executionPool.filter(poolled => poolled !== task);
      });
    }
  });
}

setInterval(tickRate, 20);

function submitTask(task) {
  return new Promise(resolve => {
    executionQueue.push({
      execute: () =>
        task().then(result => {
          resolve(result);
          return result;
        }),
      running: false
    });
  });
}

submitTask(() => axios.get("https://api.github.com/users/p1Machado/repos"))
  .then(response => response.data)
  .then(repos =>
    Promise.all(
      repos.map(repo =>
        submitTask(() =>
          axios.get(
            `https://api.github.com/repos/p1Machado/${repo.name}/commits`
          )
        )
      )
    )
  )
  .then(responses => responses.map(response => response.data))
  .then(reposCommits =>
    reposCommits.reduce((acc, commits) => {
      acc.push(...commits);
      return acc;
    }, [])
  )
  .then(commits => {
    console.log(commits.length);
  });
